const fs = require('fs');
const args = process.argv;

/*thought process:
 * load default cfg -> keyvalue pair
 *		key = command, value = value
 *
 * next make new blank keyvalue pair, only this is a double keyvalue
 * key1:(key2:value)
 * where key1 = command
 * key2 = command value
 * value = count
 *
 * go though each config individually:
 *		does it differ to default (or isn't in default)?
 *		if so then:
 *		if no key exists then add appropirate key:(key:1)
 *		if it does exist then increase key:key by 1
 *
 */

var player = args[2];

var def = {};
var vals = {};
var amnt = {};

reader('_default', true);
var plist = playerlist();
plist.forEach(function(p) {
	//console.log(p);
	reader(p,false);
});

//console.log(vals);

var topnum;
var topval;
console.log("\"VideoConfig\"");
console.log("{");
for (var key in amnt){
	if(amnt[key]>11 || !(key in def)){
		topnum = 0;
		for (var kz in vals[key]){
			if(vals[key][kz]>topnum){
				topnum = vals[key][kz];
				topval = kz;
			}
		}
		console.log("\t"+key+"\"\t\t\""+topval);
	}else{
		console.log("\t"+key+"\"\t\t\""+def[key]+"\"");
	}
}
console.log("}");


//write the config

//find top 10
/*
var topvals = [];
var topiden = [];
for(let i=0;i<10;i++){
	topvals.push(0);
	topiden.push(0);
}
var count;
for (var key in amnt){
	//console.log(vals[key]);
	count = 0;
	while((amnt[key]-Object.keys(vals[key]).length)<=topvals[count] && count <10){
		count++;
	}
	if(count>9) continue;
	topvals[count]=(amnt[key]-Object.keys(vals[key]).length);
	topiden[count]=key;
}
*/

//console.log(amnt);
//console.log(topiden);
//console.log(topvals);

//print messages
/*
	console.log("total players cfgs was: "+plist.length);
	console.log();
	for(let i=0;i<10;i++){
		console.log(topiden[i]+": "+topvals[i])
		console.log(vals[topiden[i]]);
		console.log();
	}
*/


function reader(player, dflag){
	var data = fs.readFileSync('cfgs/_video/'+player+'.txt','utf8');
	data = data.replace(/\t/g,'');
	data = data.replace(/\r/g,'');
	data = data.replace('  ', ' ');
	//console.log(data);
	var lines = data.split('\n');
	for(let i=2;i<lines.length-2;i++){
		lines[i] = lines[i].split("\"\"");
		//console.log(lines[i]);
		if(dflag){
			def[lines[i][0]]=lines[i][1];
		}else{
			if(def[lines[i][0]]!=lines[i][1]){
				if(lines[i][0] in vals){
					amnt[lines[i][0]]++;
					if(lines[i][1] in vals[lines[i][0]]){
						vals[lines[i][0]][lines[i][1]]++;
					}else{
						vals[lines[i][0]][lines[i][1]]=1;
					}
				}else{
					vals[lines[i][0]] = {};
					amnt[lines[i][0]] = 1;
					vals[lines[i][0]][lines[i][1]]=1;
				}
			}
		}

		//console.log(lines[i]);
	}
	
	return;
}

function playerlist(){
	return [
		'CeRq',
		'coldzera',
		'dream3r',
		'dupreeh',
		'f0rest',
		'FalleN',
		'GuardiaN',
		'huNter',
		'kennyS',
		'kNg',
		'magisk',
		'mantuu',
		'NAF',
		'NiKo',
		'olofmeister',
		'poizon',
		's1mple',
		'ScreaM',
		'shox',
		'SyrsoN',
		'Twistzz',
		'woxic',
		'ZywOo'];
}

