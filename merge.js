const fs = require('fs');

const args = process.argv;

if(args.length!=3){
	console.log("incorrect number of args");
	return;
}

var player = args[2];

var conf = fs.readFileSync('cfgs/'+player+'/config.cfg','utf8').split('\r\n');
var auto = fs.readFileSync('cfgs/'+player+'/autoexec.cfg','utf8').split('\r\n');

var outconf = conf;
var line;
var flag;
for(let i=0;i<auto.length;i++){
	if(auto[i].length>0 && auto[i][0]!='/'){
		line =auto[i].split(" ");
		//console.log(line);
		flag = false;
		for(let j=0;j<conf.length;j++){
			if(conf[j].split(" ")[0]==line[0]){
				//console.log(conf[j]);
				outconf[j]=auto[i];
				flag = true;
				break;
			}
		}
		if(!flag) outconf.push(auto[i]);
	}
}

fs.writeFileSync('cfgs/'+player+'.cfg',outconf.join("\r\n"));
